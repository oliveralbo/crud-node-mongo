const path = require("path");
const express = require("express");
const app = express();
const morgan = require("morgan");
const mongoose = require("mongoose");

//connecting to db
mongoose
  .connect("mongodb://localhost/crud-mongo")
  .then(db => console.log("db connected"))
  .catch(err => console.log(err));

//importing routes
//ej: import indexRoutes from './routes'
const indexRoutes = require("./routes");

//settings
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

//routes
app.use("/", indexRoutes);

// starting the server
app.listen(app.get("port"), () => {
  console.log(`escuchando puerto http://localhost:${app.get("port")}`);
});
